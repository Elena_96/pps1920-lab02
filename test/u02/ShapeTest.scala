package u02
import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class ShapeTest {

  import Exercise7._
  import Shape._

  val rectangle: Rectangle = Rectangle(10, 5)
  val square: Square = Square(10)
  val circle: Circle = Circle(5)

  @Test
  def testRectanglePerimeter(): Unit ={
    assertEquals(30, perimeter(rectangle))
  }

  @Test
  def testSquarePerimeter(): Unit ={
    assertEquals(40, perimeter(square))
  }

  @Test
  def testCirclePerimeter(): Unit ={
    assertEquals(31.41592653589793, perimeter(circle))
  }

  @Test
  def testRectangleArea(): Unit ={
    assertEquals(50, area(rectangle))
  }

  @Test
  def testSquareArea(): Unit ={
    assertEquals(100, area(square))
  }

  @Test
  def testCircleArea(): Unit ={
    assertEquals(78.53981633974483, area(circle))
  }

}
