package u02
import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class OptionalTest {
  import Exercise8.Option._

  @Test
  def testFilter(): Unit ={
    assertEquals(Some(5), filter(Some(5))(_ > 2))
    assertEquals(None(), filter(Some(5))(_ > 8))
  }

  @Test
  def testMap(): Unit ={
    assertEquals(Some(true), map(Some(5))(_ > 2))
    assertEquals(None(), map(None[Int]())(_ > 2 ))
  }

  @Test
  def testMap2(): Unit ={
    assertEquals(Some(10), map2(Some(5), Some(5))(_ + _))
    assertEquals(None(), map2(Some(5), None[Int]())(_ + _))
    assertEquals(None(), map2(None[Int](), None[Int]())(_ + _))
  }



}
