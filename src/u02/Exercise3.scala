package u02


object Exercise3 extends App {
  /*3a*/
   val f :Int => String = {
     case n if n % 2 == 0 => "even"
     case _ => "odd"
   }
    println(f(6))
    println(f(7))
    val res = 6 match {
      case n if n % 2 == 0 => "even"
      case _ => "odd"
    }
    println(res)

    def isEven (n: Int) : String = n match {
      case n if n % 2 == 0 => "even"
      case _ => "odd"
    }
    println(isEven(6))

  /*3b*/
    val empty: String => Boolean = _==""
    def negMethod (predicate: String => Boolean) : String => Boolean = {
      s => !predicate(s) //!predicate(_)
    }
    val neg: (String => Boolean) => (String => Boolean) = (p) => (s => (!p(s)))
    val notEmpty = neg(empty)

    println(notEmpty(""))
    println(neg(empty)("")) //parametro  della funzione ritornata da neg

  /*3c*/

    def genericNegMethod[A](predicate: A => Boolean) : (A => Boolean) = {
      s => !predicate(s)
    }
    val predicate: Int => Boolean = _== 0
    val r = genericNegMethod(predicate)
    println(r(0))
}
