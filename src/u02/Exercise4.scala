package u02

object Exercise4 extends App {
  val nonCurriedValComparison: (Double, Double, Double) => Boolean = (x, y, z) => x <= y && y <= z
  val curriedValComparison: Double => Double => Double => Boolean = x => y => z => x <= y && y <= z

  def nonCurriedDefComparison (x: Double, y: Double, z: Double): Boolean = x <= y && y <=z
  def curriedDefComparison (x: Double)(y: Double) (z: Double): Boolean = x <= y && y <=z

  println(nonCurriedValComparison(1, 2, 3))
  println(curriedValComparison(5)(4)(7))
  println(nonCurriedDefComparison(8, 11, 27))
  println(curriedDefComparison(1)(7)(5))
}