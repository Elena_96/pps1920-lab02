package u02

object Exercise5 extends App {
  def compose(f: Int => Int, g: Int => Int): Int => Int = {
      s => f(g(s))
  }
  println(compose(_-1,_*2) (5))

  def composeGeneric[A](f: A => A, g: A => A): A => A = {
    s => f(g(s))
  }

  println(composeGeneric[String](_ concat " you ?","Hi, " concat _) ("how are"))

  println(composeGeneric[Int](_-1,_*2) (5))
}
