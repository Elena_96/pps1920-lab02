package u02

object Exercise7 extends App {
  sealed trait Shape
  object Shape {

    case class Rectangle(high: Double, width: Double) extends Shape
    case class Square(side: Double) extends Shape
    case class Circle(radius: Double) extends Shape

    def perimeter(s: Shape): Double = s match {
      case Rectangle(a, b) => 2 * a + 2 * b
      case Square(a) => a * 4
      case Circle(r) => 2 * r * Math.PI
    }

    def area(s: Shape): Double = s match {
      case Rectangle(a, b) => a * b
      case Square(a) => a * a
      case Circle(r) => r * r * Math.PI
    }
  }
}
