package u02

object Exercise8 extends App {
  sealed trait Option[A]

  object Option extends  {
    case class None [A]() extends Option[A]
    case class Some [A](a: A) extends Option[A]

    def isEmpty[A](opt: Option[A]): Boolean = opt match {
      case None() => true
      case _ => false
    }

    def getOrElse[A, B >: A](opt: Option[A], orElse : B): B = opt match {
      case Some(a) => a
      case _ => orElse
    }

    def flatMap[A, B](opt: Option[A])(f: A => Option[B]): Option[B] = opt match {
      case Some(a) => f(a)
      case _ => None()
    }

    def filter[A](opt: Option[A])(predicate: A => Boolean): Option[A] = opt match {
      case Some(opt) if predicate(opt) => Some(opt)
      case _ => None()
    }

    def map[A, B](opt: Option[A])(mapper: A => B): Option[B] = opt match {
      case Some(opt1) => Some(mapper(opt1))
      case _ => None()
    }

    def map2[A, B, C](opt1: Option[A], opt2: Option[B])(mapper: (A, B) => C): Option[C] = (opt1, opt2) match {
      case (Some(op1), Some(op2)) => Some(mapper(op1, op2))
      case _ => None()
    }

  }
}
