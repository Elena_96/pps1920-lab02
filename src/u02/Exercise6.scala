package u02

object Exercise6 extends App {

  def fib(n: Int): Int = n match {
    case 0 => 0
    case 1 => 1
    case _ => (fib(n-1) + fib(n-2))
  }

  println((fib(0), fib(1), fib(2), fib(3), fib(4))) //not tailed recursion
}
